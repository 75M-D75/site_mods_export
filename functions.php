﻿<?php
// Inclusion du fichier contenant les variables "d'environnement"
require_once 'vars.dist.php';

// Importation phpmailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/Exception.php';
require 'phpmailer/PHPMailer.php';
require 'phpmailer/SMTP.php';

// Inclusion et initialisation de Raven (client Sentry, pour les erreurs)
require_once 'raven/lib/Raven/Autoloader.php';

$raven_client = NULL;

if ($RAVEN_DSN != '')
{
    Raven_Autoloader::register();
    $raven_client = new Raven_Client($RAVEN_DSN);

    // Raven interceptera automatiquement les exceptions, les logs error et les die
    // Note : pour que senty intercepte les extensions il faut qu'elles ne
    // soient pas catchés, ou réappeller captureException (voir fonction bdd)
    $raven_handler = new Raven_ErrorHandler($raven_client);
    $raven_handler->registerExceptionHandler();
    $raven_handler->registerErrorHandler();
    $raven_handler->registerShutdownFunction();
}

function echo_header($id)
{
    // Inclusion de la partie statique de l'en-tête dans un fichier html séparé pour plus de clarté
    include 'header.inc.html';

    // Tableau associatif définissant le menu
    // $id => [nom à afficher, lien]
    $menu = [
        'accueil' => ['Accueil', 'index.php'],
        'programme' => ['Programme', 'programme.php'],
        'intervenant' => ['Intervenants','intervenant.php'],
        'inscription' => ['Inscriptions','inscription.php'],
        'partenaires' => ['Partenaires', 'partenaires.php'],
        'edition_precedente' => ['Editions Précédentes', 'edition_precedente.php'],
        'apropos' => ['Contact & Accès', 'contact.php'],
    ];

    // Impression dynamique de chaque item du menu
    foreach ($menu as $key => $value)
    {
        echo '<ul class="nav navbar-nav">' . PHP_EOL; // Fixe

        if ($key == $id)
        {
            // Si c'est la page sur laquelle on est on fait un active
            echo '<li class="active">';
        }
        else
        {
            // Sinon un li normal
            echo '<li>';
        }
        // Création du lien ($value[1] = url et $value[0] = texte à afficher)
        echo '<a href="' . $value[1] . '">' . $value[0] . '</a></li>' . PHP_EOL;
        echo '</ul>' . PHP_EOL;
    }

    echo '</div>
          </div>
        </nav>

        </header>
    ';

}

function echo_footer()
{
    // Inclusion de la partie statique du footer dans un fichier html séparé pour plus de clarté
    include 'footer.inc.html';
}


//classe pour sécuriser nos pages
class Securite

{
    // Données entrantes
    public static function bdd($string)
    {
        if(ctype_digit($string))// On regarde si le type de string est un nombre entier (int)
        {
            $string = intval($string);
        }
        else // Pour tous les autres types
        {
            $string = mysql_real_escape_string($string);
            $string = addcslashes($string, '%_');
        }

        return $string;
    }

    // Données sortantes
    public static function html($string)
    {
        return htmlentities($string);
    }

}

/**
 * Ouvre une connection à la BDD et la retourne
 */
function make_db_connection()
{
    try
    {
        global $MYSQL_PDO_LINE, $MYSQL_USER, $MYSQL_PASSWORD;
        return new PDO($MYSQL_PDO_LINE, $MYSQL_USER, $MYSQL_PASSWORD);
    }
    catch(PDOException $e)
    {
        global $raven_client;
        // Envoyer l'exception à Senty pour notif
        // Penser à tester que le client peut être null
        if ($raven_client){
            $raven_client->captureException($e);
        } else {
            echo 'Attention : Rapportage d\'erreur désactivé<br>';
        }
        echo '<p style="font-size:2em;text-align:center;"><strong>Désolé, une erreur technique est survenue</strong><br>';
        echo 'Nous ne pouvons pas traiter votre demande actuellement. Merci de réessayer plus tard<br>';
        echo 'Notre équipe a été prévenue et interviendra dans les plus brefs délais<br>';
        echo 'Si le problème persiste, contactez nous à : <a href="mailto:contact@montpellier-omics-days.fr">contact@montpellier-omics-days.fr</a>';
        echo '</p></body></html>';
        die(1);
        // TODO Ne pas afficher les erreurs en prod
    }
}

/**
 * Envoie un mail
 * @param string $recipient Adresse mail du destinataire
 * @param string $subject Sujet du message
 * @param string $message Message à envoyer formatté en HTML (converti automatiquement en php)
 * NOTE : Si $subject ou $message sont sourcés de l'utilisateur, penser à les sanitiser
 * Lève une exception en cas d'échec avec le message d'erreur de phpmailer
 */
function send_mail($recipient, $subject, $message) {
    try
    {
        global $MAIL_SEND, $MAIL_HOST, $MAIL_PORT, $MAIL_USER, $MAIL_PASSWORD,
        $MAIL_SECURITY, $MAIL_FROM_ADDR, $MAIL_FROM_NAME;

        if (!$MAIL_SEND){
            // Si on est configuré pour ne pas envoyer les mails, on fait comme si ça avait marché.
            return;
        }

        $mail = new PHPMailer(true);

        $mail->isSMTP(); // Indiquer à phpmailer qu'il doit envoyer en SMTP
        $mail->Host = $MAIL_HOST;
        $mail->Port = $MAIL_PORT;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = $MAIL_SECURITY;
        $mail->Username = $MAIL_USER;
        $mail->Password = $MAIL_PASSWORD;
        $mail->CharSet = 'utf-8';

        $mail->setFrom($MAIL_FROM_ADDR, $MAIL_FROM_NAME);
        $mail->addAddress($recipient);
        $mail->Subject = $subject;
        $mail->msgHtml($message);

        $mail->send();
        
    } catch (Exception $e) {
        // Envoyer silencieusement l'exception à Senty pour notif
        // Penser à tester que le client peut être null
        // On joint le message d'erreur de phpmailer
        global $raven_client;
        if ($raven_client) {
            $raven_client->captureException($e, array(
                'extra' => array(
                    'phpmailer_message' => $mail->ErrorInfo
                )
            ));
        } else {
            // On considère que si sentry est off on est en dev
            die($mail->ErrorInfo);
        }
    }
}

/**
 * Formattage de chaine de caractères avec des arguments nommés
 * Source : http://php.net/manual/fr/function.sprintf.php (CTRL-F -> "named")
 * version of sprintf for cases where named arguments are desired (php syntax)
 *
 * with sprintf: sprintf('second: %2$s ; first: %1$s', '1st', '2nd');
 *
 * with sprintfn: sprintfn('second: %second$s ; first: %first$s', array(
 *  'first' => '1st',
 *  'second'=> '2nd'
 * ));
 *
 * @param string $format sprintf format string, with any number of named arguments
 * @param array $args array of [ 'arg_name' => 'arg value', ... ] replacements to be made
 * @return string|false result of sprintf call, or bool false on error
 */
function sprintfn ($format, array $args = array()) {
    // map of argument names to their corresponding sprintf numeric argument value
    $arg_nums = array_slice(array_flip(array_keys(array(0 => 0) + $args)), 1);

    // find the next named argument. each search starts at the end of the previous replacement.
    for ($pos = 0; preg_match('/(?<=%)([a-zA-Z_]\w*)(?=\$)/', $format, $match, PREG_OFFSET_CAPTURE, $pos);) {
        $arg_pos = $match[0][1];
        $arg_len = strlen($match[0][0]);
        $arg_key = $match[1][0];

        // programmer did not supply a value for the named argument found in the format string
        if (! array_key_exists($arg_key, $arg_nums)) {
            user_error("sprintfn(): Missing argument '${arg_key}'", E_USER_WARNING);
            return false;
        }

        // replace the named argument with the corresponding numeric one
        $format = substr_replace($format, $replace = $arg_nums[$arg_key], $arg_pos, $arg_len);
        $pos = $arg_pos + strlen($replace); // skip to end of replacement for next iteration
    }

    return vsprintf($format, array_values($args));
}

function check_date() {
    $date = getdate();

    if($date['year'] > 2020 || $date['mon'] > 2 || ($date['mon'] == 2 && $date['mday'] > 6) || ($date['mon'] == 2 && $date['mday'] == 6 && $date['hours'] > 12)) {
        // header("Location: index.php?inscriptions_fermees=1");
    }
}

?>