<?php
/* Fichier de distribution
   Ce fichier rassemble les variables d'éxécution pouvant varier d'une installation à l'autre
   Copier ce fichier dans vars.php et remplacer les valeurs pour correspondre a l'installation locale
   NE PAS ajouter vars.php à git, uniquement vars.dist.php
   Dans les pages, vars.php devrait être inclus avec require ou require_once pour empêcher l'affichage de la page si vars.php est absent.
   TODO s'assurer que le fichier est inaccessible depuis l'extérieur
*/

$MAINTEANCE			= false;

$MYSQL_HOST 		= 'localhost'; // Hôte mysql
$MYSQL_DB 			= 'omics'; // Base de données mysql
$MYSQL_USER 		= 'root'; // Utilisateur mysql
$MYSQL_PASSWORD 	= ''; // Mot de passe mysql ('' pour pas de mdp)

$MAIL_SEND 			= true; // Envoyer les mails ou non. Si false les autres paramètres ne sont pas lus
$MAIL_HOST 			= 'ssl0.ovh.net'; // Serveur SMTP
$MAIL_PORT 			= 465;
$MAIL_USER 			= 'contact@montpellier-omics-days.fr'; // Utilisateur SMTP
$MAIL_PASSWORD 		= 'pass'; // Mot de passe SMTP
$MAIL_SECURITY 		= 'ssl'; // Mode de sécurité SMTP : ssl ou tls
$MAIL_FROM_ADDR 	= 'contact@montpellier-omics-days.fr'; // Adresse qui expédie le mail
$MAIL_FROM_NAME 	= 'Montpellier Omics Days'; // Nom à afficher

$RAVEN_DSN 			= ''; // DSN pour Sentry. Si vide l'init de Sentry sera passée

$RECAPTCHA_KEY 		= '6Lf1dDgUAAAAANzTfHmIm7U-PKFKpxk13HLe0uC-'; // Clé d'API pour reCaptcha
// Les valeurs possibles sont 6Lf1dDgUAAAAANzTfHmIm7U-PKFKpxk13HLe0uC- pour localhost
// et 6Le6xzkUAAAAAGeOaTQjQotvwiP8Y8pDhZRoJo5C pour les environnements de test et prod
// NE PAS DISTRIBUER PUBLIQUEMENT LA CLÉ LOCALHOST sous peine de malédiction sur 5 générations

$RECAPTCHA_SECRET 	= '6Lf1dDgUAAAAABtOG0aNJGNuqwyR0xHedV_3-oNL'; // Clé secrète pour recaptcha
// Les valeurs possibles sont 6Lf1dDgUAAAAABtOG0aNJGNuqwyR0xHedV_3-oNL pour localhost
// et 6Le6xzkUAAAAANV9uBOwRhZBj2Pdf-9OfylUkJRf pour les environnements de test et de prod

// Génération automatique de certaines valeurs

$MYSQL_PDO_LINE 	= 'mysql:host=' . $MYSQL_HOST . ';dbname=' . $MYSQL_DB;

?>