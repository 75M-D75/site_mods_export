# Site web des MODs 2018

Le site est prévu pour php 5 et 7, utilise Mysql comme BDD, phpmailer pour SMTP, Sentry pour rapporter les erreurs automatiquement, et boostrap comme base pour le design.

Le dossier `public` est à monter en document root, tout ce qui est situé au dessus doit être caché.

## Dossiers & fichiers

* `raven/` : lib, client pour envoyer les erreurs à Sentry
* `phpmailer/` : lib, pour envoyer les mails via SMTP
* `public/` : pages et assets publics
* `mails/` : templates d'emails
* `header.inc.php:` et `footer.inc.php` : Début et fin de page réutilisées dans toutes les pages publiques
* `functions.php` : fonctions communes : génération menu, connexion BDD, rapportage d'erreurs à sentry, envoi mail
* `omics.sql` : fichier SQL à importer dans une BDD vierge pour créer une BDD utilisable par le site. Contient aussi la description des évènements pour l'édition 2018
* `vars.dist.php` : fichier modèle contenant les différentes clés et variables pour la BDD, le serveur SMTP et Sentry. A copier dans vars.php et compléter les valeurs manquantes pour le déploiement.
