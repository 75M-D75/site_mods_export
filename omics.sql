-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  ven. 17 nov. 2017 à 11:09
-- Version du serveur :  10.1.25-MariaDB
-- Version de PHP :  7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `omics`
--

-- --------------------------------------------------------

--
-- Structure de la table `Evenement`
--

CREATE TABLE `Evenement` (
  `id` int(1) NOT NULL,
  `nom` varchar(355) NOT NULL,
  `moment` enum('matin','aprem', 'journee'),
  `type` enum('conference','workshop') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Evenement`
--

INSERT INTO `Evenement` (`ID`, `nom`, `moment`, `type`) VALUES
(1, 'conferences', 'journee', 'conference'),
(2, 'R', 'matin', 'workshop'),
(3, 'Python', 'matin', 'workshop'),
(4, 'Requetage', "matin", 'workshop'),
(5, 'Phylogenie', "aprem", 'workshop'),
(6, 'Alignement', "aprem", 'workshop'),
(7, 'Machine_Learning', "aprem", 'workshop');


-- --------------------------------------------------------

--
-- Structure de la table `Inscrit`
--

/*DROP TABLE participe;
DROP TABLE evenement;
DROP TABLE inscrit;*/

CREATE TABLE `Inscrit` (
  `nom` varchar(355) NOT NULL,
  `prenom` varchar(355) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `statut` enum('etudiant','docteur','post-doctorant','enseignant','enseignant_chercheur','prive','ingenieur','autre') NOT NULL,
  `droit_image` int(1) DEFAULT NULL,
  `civilite` enum('Monsieur','Madame','') DEFAULT NULL,
  `abstract` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*ALTER TABLE `Inscrit` 
  MODIFY `civilite` enum('Monsieur','Madame','');*/

/*ALTER TABLE `Inscrit` 
  ADD `pays` VARCHAR (50) DEFAULT 'France',
  ADD `affiliation` VARCHAR (200) DEFAULT NULL;*/

--
-- Déchargement des données de la table `Inscrit`


-- --------------------------------------------------------

--
-- Structure de la table `Participe`
--

CREATE TABLE `Participe` (
  `mail_inscrit` varchar(100) NOT NULL,
  `id_evenement` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Participe`
--


--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Evenement`
--
ALTER TABLE `Evenement`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `Inscrit`
--
ALTER TABLE `Inscrit`
  ADD PRIMARY KEY (`mail`);

--
-- Index pour la table `Participe`
--
ALTER TABLE `Participe`
  ADD PRIMARY KEY (`mail_inscrit`,`id_evenement`);

--
-- Contraintes pour la table `Participe`
--
ALTER TABLE `Participe`
  ADD CONSTRAINT `Participe_ibfk_1` FOREIGN KEY (`mail_inscrit`) REFERENCES `Inscrit` (`mail`) ON DELETE CASCADE,
  ADD CONSTRAINT `Participe_ibfk_2` FOREIGN KEY (`id_evenement`) REFERENCES `Evenement` (`ID`) ON DELETE RESTRICT;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



SELECT mail, id_evenement, e.nom from Inscrit, Participe, Evenement e
WHERE id_evenement=id and mail=mail_inscrit;

SELECT COUNT(id_evenement), id_evenement, e.nom from Inscrit, Participe, Evenement e
WHERE id_evenement=id and mail=mail_inscrit GROUP BY id_evenement;