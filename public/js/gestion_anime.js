jQuery(document).ready(function($){
    //Intialisation
    $("#text-sns").removeClass("text-shadow-pop-br");
    $("#logo").removeClass("rotate-in-center");

    //verifie si un element est entierement visible
    function checkvisible( elm ) {
        var vpH = $(window).height(), // Viewport Height
            st = $(window).scrollTop(), // Scroll Top
            y = $(elm).offset().top + $(elm).height();

        return (y > (vpH + st));
    }

    window.addEventListener('scroll', function() {
        // si il est visible dans la page
        if( !checkvisible( "#logo" ) ){
            // ajout de la classe pour lancer l'animation
            $("#text-sns").addClass("tracking-in-expand");
            $("#logo").addClass("rotate-in-center");
        }
    });

    /* Photo etudiante */

    var visible_big_photo = false;

    function apparition_big_picture(){
        if(visible_big_photo){
            $("#container-img-students-big").css("display","none");
            $("#container-img-students-big").removeClass("scale-up-center");
            visible_big_photo = false;
        }
        else{
            $("#container-img-students-big").css("display","initial");
            $("#container-img-students-big").addClass("scale-up-center");
            visible_big_photo = true;
        }
    }

    $("#container-img-students-big").click(function(){
        apparition_big_picture();
    });

    $("#student_img").click(function(){
        apparition_big_picture();
    });
});