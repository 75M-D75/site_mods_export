<?php
//on inclut le fichier contenant les fonctions php
require("../functions.php");

//on mets en place le header
echo_header("apropos");
?>

<noscript>
    <div class="alert alert-danger fade in">
        <a href="#" data-dismiss="alert">&times;</a>
        <strong>Veuillez activer javascript!</strong> 
    </div>
</noscript>


<!-- <iframe id="inlineFrameExample"
    title="Inline Frame Example"
    width="300"
    height="200"
    src="https://traitbot.000webhostapp.com/map/">
</iframe>
 -->
<div id="contact-content">
  <div class="well">
    <ul class="nav nav-tabs" id="myTab">
      <li class="active"><a data-toggle="tab" href="#menu4"><i class="icon icon-car"></i> Accès </a></li>
      <li><a data-toggle="tab" href="#menu3"><i class="icon icon-compass"></i> Localisation</a></li>
      <li><a data-toggle="tab" href="#menu5"><i class="icon icon-envelope"></i> Mail </a></li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div id="menu4" class="tab-pane fade text-center active in" style="overflow-x:auto;white-space:pre-wrap; white-space: -moz-pre-wrap !important; word-wrap:break-word; white-space: normal;margin-top:10px;">

        <address style="font-size:1.3em;">
          <strong>Université de Montpellier</strong><br>
          Faculté des sciences<br>
          Place Eugène Bataillon<br>
          34090 Montpellier<br>
        </address>
        <!-- <div class="message message-warning">
          <strong>Habitués, attention !</strong><br>
          L'entrée de la faculté des sciences est en travaux et l'accès est par conséquent modifié.<br>Il y a aussi moins de stationnement disponible à l'extérieur du campus qu'à l'habitude.
        </div> -->
        <img src="Images/plan2020.png" style="display: block; margin: auto;"></img>
        <strong class="text-center">Depuis l'arrêt de la ligne 1 de tramway "Universités des Sciences et Lettres" :</strong>
        <p class="text-center">
          <br>
          A la sortie du tramway, traversez la voie de sortie de bus derrière
          l'arrêt puis prenez le chemin pavé et jalonné d'arbres.<br>
          Marchez jusqu'à atteindre un rond-point (rouge et en forme de donut).
          Traversez la rue et prenez sur la droite dans le cheminement piéton
          marqué "Nouvel accès Truel" qui longe les barrières de chantier.<br>
          Vous allez arriver à l'entrée de la faculté des sciences.
          Le bâtiment 7 (bâtiment administratif) dans lequel se déroulent
          les conférences du 7 février est face à l'entrée.
        </p>
        <br>
        <strong class="text-center">Depuis la gare Montpellier Saint-Roch :</strong>
        <p class="text-center">
          <br> 
          A la sortie de la gare, dirigez-vous vers la station de la ligne 1 du
          tramway, face à l'entrée principale de la gare.<br>
          Prenez le tramway 1 direction Mosson. Descendez à l'arrêt "Universités 
          des Sciences et Lettres" (6ème arrêt depuis la gare). <br>
          
          A la sortie du tramway, traversez la voie de sortie de bus derrière
          l'arrêt puis prenez le chemin pavé et jalonné d'arbres.<br>
          Marchez jusqu'à atteindre un rond-point (rouge et en forme de donut).
          Traversez la rue et prenez sur la droite dans le cheminement piéton
          marqué "Nouvel accès Truel" qui longe les barrières de chantier.<br>
          Vous allez arriver à l'entrée de la faculté des sciences.
          Le bâtiment 7 (bâtiment administratif) dans lequel se déroulent
          les conférences du 7 février est face à l'entrée.<br><br>

          Pour venir en transports en commun depuis un autre endroit de
          Montpellier, vous trouverez un
          <a href="http://www.tam-voyages.com/ri/?rub_code=4&amp;noscript=0"> calculateur d'itinéraires</a>
          sur le site de la TaM. Utilisez la destination "Universités des Sciences et Lettres (Arrêt)".

        </p>
      </div>
      <div id="menu3" class="tab-pane fade" style="text-align:center;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d721.9492661208437!2d3.8615146804604006!3d43.63158254893843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b6aede6a2f7ec3%3A0x14715e24ed076f4f!2sFacult%C3%A9+des+Sciences+de+Montpellier!5e0!3m2!1sfr!2sfr!4v1510617812718" frameborder="0" style="border:0; width: 100%; height: 50vh;" allowfullscreen></iframe>
      </div>
      <div id="menu5" class="tab-pane fade" style="overflow-x:auto;white-space:pre-wrap; white-space: -moz-pre-wrap !important; word-wrap:break-word; white-space: normal;margin-top:10px;">
        <div class="mr-auto text-center">
            <i class="fa fa-envelope-o sr-contact" data-sr-id="9" style="width:100px;visibility: visible; font-size:3em; color: #79c645; margin-bottom: 10px;"></i>
            <p style="font-size: 1.3em;">
              <a href="mailto:contact@montpellier-omics-days.fr">contact@montpellier-omics-days.fr</a>
            </p>
          </div>
      </div>
    </div>
  </div>
</div>

<?php
echo_footer();
?>
