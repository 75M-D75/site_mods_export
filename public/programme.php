<?php
//on inclut le fichier contenant les fonctions php
require("../functions.php");

//on mets en place le header
echo_header("programme");
?>

<div id="content_programme">
    <h1>Formations <small>Jeudi 6 Février</small></h1>
    <!-- <h4><p> Vous pouvez télécharger le programme détaillé des workshops en cliquant <a href="Images/programme_WS.pdf">ici</a></p></h4>
    <div class="line" style="margin-top:-20px;"></div> -->
    
    <!-- buiding -->
    <!-- <div id=container>
      <img src="https://pbs.twimg.com/profile_images/459505368234606592/nuxaaQoo_400x400.jpeg"" id="avatar" width="250" draggable="false"/>

      <div id=text>
        <h1>A venir…</h1>
        <p title="(I guess)">(Bientôt)</p>
      </div>
    </div> -->

    <!-- Coming soon -->
    <!-- <div class="ctn_page">
        <div class="page">
            <h1>Bientôt</h1>
        </div>
    </div> -->

    <table class="prog-ws table table-bordered table-striped">
        <tr>
            <th>Heure</th>
            <th>Programme 1</th>
            <th>Programme 2</th>
            <th>Programme 3</th>
        </tr>
        <tr>
            <td>9h00</td>
            <td>Introduction aux langages de programmation (R), Salle 6.04</td>
            <td>Introduction aux langages de programmation (Python/Bash), Salle 36.206</td>
            <td>Introduction au langage SQL (base de données), Salle 36.208</td>
        </tr>
        <tr>
            <td>10h30</td>
            <td class="transition-row" colspan="6">Pause café</td>
        </tr>
        <tr>
            <td>10h45</td>
            <td>Suite Introduction langages de programmation (R), Salle 6.04</td>
            <td>Introduction aux langages de programmation (Python/Bash), Salle 36.206</td>
            <td>Introduction au langage SQL (base de données), Salle 36.208</td>
        </tr>
        <tr>
            <td>12h30</td>
            <td class="transition-row" colspan="4">Pause midi</td>
        </tr>
        <tr>
            <td>14h00</td>
            <td>Phylogénie, Salle 6.04</td>
            <td>Alignement/Assemblage, Salle 36.206</td>
            <td>Introduction au machine learning, Salle 36.208</td>
        </tr>
        <tr>
            <td>15h30</td>
            <td class="transition-row" colspan="4">Pause café</td>
        </tr>
        <tr>
            <td>15h45</td>
            <td>Phylogénie, Salle 6.04</td>
            <td>Alignement/Assemblage, Salle 36.206</td>
            <td>Introduction au machine learning, Salle 36.208</td>
        </tr>
    </table>


    <h1>Conférences <small>Vendredi 7 février</small> (Amphithéâtre Dumontet) </h1>
    <!-- <div class="line" style="margin-top:-20px;"></div>
    <p class="alert alert-info" style="margin-top: 10px;"><i class="icon icon-download" aria-hidden="true"></i> Le programme est également disponible au format pdf : <a href="Images/programme.pdf">cliquez ici</a></p> -->
    <!-- Comming soon -->
    <!-- <div class="ctn_page">
        <div class="page">
            <h1>Bientôt</h1>
        </div>
    </div> -->

    <div>
        <table class="table table-striped table-bordered prog-conf">
            <tr>
                <th>Heure</th>
                <th>Type</th>
                <th>Intervenant</th>
                <th>Titre</th>
            </tr>
            <tr>
                <td>8h00</td>
                <td class="transition-row" colspan="3">Accueil</td>
            </tr>
            <tr>
                <td>8h30</td>
                <td>Discours d'ouverture</td>
                <td>Représentants</td>
            </tr>
            <tr>
                <td>Thématique</td>
                <td class="transition-row" colspan="3">MULTI-OMIQUES</td>
            </tr>
            <tr>
                <td>9h00</td>
                <td>Talk</td>
                <td>Sébastien Déjean, Institut de Mathématiques de Toulouse</td>
                <td><a href="./intervenant.php">Méthodes statistiques multivariées pour l'exploration et l'intégration de données omiques</a></td>
            </tr>
            <tr>
                <td>9h45</td>
                <td>Talk</td>
                <td>Harold Duruflé, INRA Laboratoire des intéractions Plantes-Microorganismes (LIPM), Toulouse</td>
                <td><a href="./intervenant.php">Unsupervised methods on heterogeneous omics data highlights the emergence of novel molecular and physiological strategies of drought response in sunflower</a></td>

            </tr>
            <tr>
                <td>10h30</td>
                <td>Talk</td>
                <td>Christine Brun, TAGC INSERM Aix-Marseille, Marseille</td>
                <!-- <td><a href="./intervenant.php">En attente</a></td> -->
                <td>Yet another interaction network : 3'UTRs, lncRNAs and RNA-binding proteins</td>

            </tr>
            <tr>
                <td>11h15</td>
                <td class="transition-row" colspan="3">Posters</td>
            </tr>
            <tr>
                <td>12h00</td>
                <td class="transition-row" colspan="3">Repas (1h45)</td>
            </tr>
            <tr>
                <td>14h00</td>
                <td>Talk</td>
                <td>Aleksandra Pekowska, Nencki institute of Experimental Biology, Varsovie (Pologne)</td>
                <td><a href="./intervenant.php">Chromatin conformation assays - normalisation, feature extraction and comparative analysis</a></td>

            </tr>
            <tr>
                <td>Thématique</td>
                <td class="transition-row" colspan="3">EVOLUTION</td>
            </tr>
            <tr>
                <td>14h45</td>
                <td>Talk</td>
                <td>Nicolas Lartillot, LBBE, Lyon</td>
                <td><a href="./intervenant.php">Bayesian inference and probabilistic modeling in phylogenetics and macroevolution</a></td>

            </tr>
            <tr>
                <td>15h30</td>
                <td>Talk</td>
                <td>Guillaume Beslon, LIRIS-Lyon, Lyon</td>
                <td><a href="./intervenant.php">Evolution expérimentale in silico avec la plate-forme Aevol</a></td>

            </tr>
            <tr>
                <td>16h15</td>
                <td class="transition-row" colspan="3">Pause café</td>

            </tr>
            <tr>
                <td>16h45</td>
                <td>Talk</td>
                <td>Antoine Paccard, McGill University Montréal, Montréal (Canada)</td>
                <td><a href="./intervenant.php">Signatures génomiques parallèle de sélection dans des environnements changeants</a></td>

            </tr>
            <tr>
                <td>17h30</td>
                <td>Talk</td>
                <td>Frédéric Delsuc, ISEM - CNRS, Montpellier</td>
                <td><a href="./intervenant.php">Rubrique des chiens écrasés : assemblage hybride de génomes de mammifères de qualité à partir de spécimens récoltés le long des routes</a></td>

            </tr>
            <tr>
                <td>18h15</td>
                <td class="transition-row" colspan="3">Clôture</td>
            </tr>
        </table>
    </div>

    <!-- <h2>Programmes des workshops</h2>
    <h3>Manipulation de données via R <small>Matin</small></h3>
    <p>
    Ce workshop s'adresse aux personnes n'ayant jamais manipulé R ou souhaitant revoir les bases du logiciel.
    </p>

    <h3>Analyse de données via R <small>Après-midi</small></h3>
    <p>
    Ce workshop vise à présenter quelques notions de manipulation de jeux de données sous R, ainsi qu'à détailler l'implémentation de quelques méthodes statistiques courantes (chi-2, ANOVA I). 
    C'est une suite directe du workshop du matin : son contenu s'adaptera en fonction de l'avancement de ce dernier.
    </p>

    <h2>Unix/NGS</h2>
    <h3>Lignes de commandes UNIX <small>Matin</small></h3>
    <p>
    Cet atelier est à destination des débutants non familier avec l'usage d'un terminal de commande. 
    L'accent sera mis sur les commandes de bases permettant d'évoluer dans un environnement Unix (Linux) sans l'aide d'une interface graphique.
    </p>

    <h2>Python</h2>
    <h3>Introduction au language Python <small>Après-Midi</small></h3>
    <p>Ce workshop s'adresse aux néophytes en programmation. 
    Il permettra d'explorer les bases du langage de programmation python afin d'obtenir les rudiments nécessaire pour pouvoir créer son premier script.
    </p> -->
    
    

</div>


<?php
echo_footer();
?>
