$(window).on('load', function() {

	var header = document.querySelector("#navigation header");
	var nav = document.querySelector("#navigation");
	var mst = document.querySelector("#menu-static-top");

	function scrolled(){
		var windowHeight = document.body.clientHeight,
			currentScroll = document.body.scrollTop || document.documentElement.scrollTop;
		mst.className = (currentScroll >= windowHeight - nav.offsetHeight ) ? "navbar navbar-info navbar-fixed-top scrolled" : "navbar navbar-info";
	}

	addEventListener("scroll", scrolled, false);
});