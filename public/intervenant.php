<?php
    // Note : aucun contenu avant le ?php pour pouvoir modifier les headers si besoin
    //on inclut le fichier contenant les fonctions php
require("../functions.php");

if( $MAINTEANCE ){
	header('Location: page_not_found.php');
	exit();
}
    //on mets en place le header
echo_header("intervenant");
?>
<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Roboto');
	.body {background-color: #0D1B2B; overflow-x: hidden; font-family: roboto; -webkit-font-smoothing: antialiased; margin: 0;}
	.flex { display: -webkit-flex; display: flex; -webkit-flex-direction: row;  flex-direction: row; -webkit-justify-content: flex-start; justify-content: flex-start;}

	.slider-wrapper div {position: relative;}
	.slider-wrapper {margin-top: 5vw;  margin-left: 11vw;}
	.slide-image {height: 24vw;}
	.slide-image img {width: 24vw; cursor: pointer;}
	.slide-content {width: 25vw; color: #fff; padding:3vw 18vw 3vw 9vw;}
	.slide-date {color: #0a8acb; font-size: 1.1vw; font-weight: 400; letter-spacing: 0.1vw; padding-bottom: 1.4vw;}
	.slide-date a{
		/*text-align: justify;
		text-justify: inter-word;*/
		    color: deeppink;
	}
	.name {
	    font-size: 1.8em;
	}
	.slide-title {font-size: 1.4vw; font-weight: 400; letter-spacing: 0.1vw; line-height: 1.55vw; padding-bottom: 1.8vw;}
	.slide-title{
		color: gold;
		
	}
	.slide-text {font-size: 0.80vw; line-height: normal; opacity: 0.8; padding-bottom: 4vw; font-size: 1.2em;}
	.slide-text {
		text-align: justify;
		text-justify: inter-word;
		text-justify: inter-word;
	    overflow: auto;
	    max-height: 350px;
	    padding-right: 5px;
	}


	.slide-more {font-weight: 400; letter-spacing: 0.1vw; float: left; font-size: 0.9vw;}
		.slide-bullet {width: 0.5vw; height: 0.5vw; background-color: #0b8bcc; border-radius: 200%; position: relative; margin-left: 1.2vw;}
		.slide-nav {margin-left: 64vw; margin-top: -5.5vw;}

		div.overlay-blue {width: 100%; height: 100%; position: absolute; top: 0; transition: 0.5s ease all;}
		div.overlay-blue:hover {background-color: rgba(13, 27, 43, 0.5);}

		.arrows{width: 3.5vw; margin-top: -5.8vw; margin-left: 72vw; position: relative;}
		.arrow {display: inline-block; position: absolute; width: 1.2vw; height: 1.2vw; background: transparent; text-indent: -9999px; border-top: 0.15vw solid #fff; border-left: 0.15vw solid #fff; transition: all .1s ease-in-out; text-decoration: none; color: transparent;
		}
		.arrow:hover {border-color: #0A8ACB; border-width: 0.25vw;
		}
		.arrow:before {display: block; height: 200%; width: 200%; margin-left: -50%; margin-top: -50%; content: ""; transform: rotate(45deg);}
		.arrow.prev {transform: rotate(-45deg); left: 0;}
		.arrow.next {transform: rotate(135deg); right: 0;}
		.body{
			margin-top: -17px;
			background-color: #0D1B2B;
			overflow-x: hidden;
			font-family: roboto;
			-webkit-font-smoothing: antialiased;
			/*border-radius: 7px 7px;*/
			/*box-shadow: 5px 5px 5px black;*/
			padding-bottom: 100px;
		}
		*{
			-webkit-box-sizing: inherit;
			-moz-box-sizing: inherit;
			box-sizing: inherit;
		}
		#content{
			max-width: inherit;
		}
	</style>

	<div id="content">
		<div class="body"> 
			<div class="slider">
				<div class="slider-wrapper flex">
					<div class="slide flex">
						<div class="slide-image slider-link prev"><img src="Images/orateurs_photos/Harold_Durufle.jpg"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Harold DURUFLE</div>
							<div class="slide-date"><a href="https://www6.toulouse.inra.fr/lipm/Presentation">Institut : INRA Laboratoire des Intéractions Plantes-Microorganismes (LIPM), Toulouse, France.</a></div>
							<div class="slide-title">Unsupervised methods on heterogeneous omics data highlights the emergence of novel molecular and physiological strategies of drought response in sunflower</div>
							<div class="slide-text">Climate change is a major concern because of its potential effects on biodiversity and the agricultural sector. The domesticated sunflower, Helianthus annuus L., is the fourth most important oilseed crop in the world [1], is cultivated as hybrids and is promising for agriculture adaptation because it can maintain stable yields across a wide variety of environmental conditions, especially during drought stress [2]. 
To highlight the molecular processes related to the drought adaptation strategies, we studied the responses of eight parental lines (four males and four females) and their 16 hybrids cultivated on the outdoor high-throughput automated phenotyping platform Heliaphen [3] where two water regimes were implemented and ecophysiological traits were measured. Leaf samples from 144 samples were collected for multi-omics analyses including label-free shotgun analysis and protein identification and quantification. 
Statistical learning methods without a priori allowed us to reduce this complex molecular system [4] and to model proteomics and phenomics relationships to identify molecular players of heterotic behaviors and their roles during drought stress. We showed dramatic behavioral differences between hybrids and parental lines with solid interaction with water deficit that are exemplified by some highly interesting protein candidates. Further analysis of these genes or proteins highlights the impact of genetic variation on regulatory networks that could be selected to improve heterotic groups and breed for drought stress tolerance. 
Current integration of eco-physiological, proteomics, transcriptomics and metabolomics levels in order to achieve a more holistic view will be presented and discussed.

</br>
References :
[1] FAS. USDA. Oilseeds: World Markets and Trade. (2019).
[2] H. Badouin, et al., The sunflower genome provides insights into oil metabolism, flowering and Asterid evolution, Nature. 546 (2017) 148–152. doi:10.1038/nature22380.
[3] F. Gosseau et al., Heliaphen, an outdoor high-throughput phenotyping platform designed to integrate genetics and crop modeling. Frontiers in plant science, 9 (2018), 1908. doi: 10.3389/fpls.2018.01908
[4] H. Duruflé, et al., A powerful framework for an integrative study with heterogeneous omics data: from univariate statistics to multi-block analysis. BioRxiv. (2019) 357921v2. doi:10.1101/357921.


							</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>
					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/Sebastien_Dejean.jpg"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Sébastien DÉJEAN</div>
							<div class="slide-date"><a href="https://www.math.univ-toulouse.fr/spip.php?article11">Institut de Mathématiques de Toulouse</a></div>
							<div class="slide-title">Méthodes statistiques multivariées pour l'exploration et l'intégration de données omiques</div>
							
							<div class="slide-text">La présentation dresse un panorama des méthodes statistiques multivariées pour l'exploration et l'intégration de données omiques, de l'analyse en composantes principales (ACP) aux extensions récentes des méthodes Projection to Latent Structures (PLS) pour les tableaux multiples. Ces méthodes sont illustrées sur des données réelles traitées à l'aide du package mixOmics pour le logiciel R.</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>	
					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/Antoine_Paccard.jpeg"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Antoine PACCARD</div>
							<div class="slide-date"><a href="http://www.mcgillgenomecentre.org">McGill University, Montréal, Québec, Canada</a></div>
							<div class="slide-title">Signatures génomiques parallèle de sélection dans des environnements changeants</div>
							<div class="slide-text">L'évolution parallèle est considérée comme une preuve solide du travail de sélection naturelle. Cependant, étonnamment peu de travaux ont étudié le processus d'évolution parallèle dans des écosystèmes réels. La plupart des études se limitent à des signatures historiques de sélection dans des populations déjà adaptées à de nouveaux environnements. Ici, afin de documenter la sélection naturelle en action, nous avons étudié un ensemble de populations d'épinoches à trois épines (Gasterosteus aculeatus) habitant des estuaires à bouchon vaseux le long de la côte centrale Californienne. Chaque année, de fortes pluies provoquent un débit d'eau suffisamment élevé pour percer les bouchons vaseux, reliant ainsi les cours d'eau à l'océan. Les nouveaux bouchons se reforment généralement en quelques semaines ou quelques mois, isolant de nouveau les populations dans les estuaires. Ces événements de rupture provoquent des changements importants et souvent extrêmement rapides des conditions abiotiques et biotiques, y compris des changements dans l'abondance de prédateurs. Nous avons cherché à savoir si cette forte variation environnementale temporelle pourrait maintenir la variation intra-population tout en érodant la divergence adaptative entre les populations qui serait causée par la variation de la sélection spatiale. Nous avons utilisé des marqueurs génétiques neutres pour explorer la structure des populations, puis analysé comment les traits de protection de l’épinoche, les gènes associés Eda et Pitx1 ainsi que la composition élémentaire (% P) varient au sein des populations. De plus, nous avons utilisé les données de séquençage whole-genome (Pool-WGS) pour suivre les changements de fréquence alléliques sur une saison et rechercher des signatures de sélection parallèles. Nos résultats présentent des signaux de divergence dans les traits de défense des épinoches ainsi qu’au niveau du gène Eda, associés au régime de prédation. De plus, nous avons découvert des preuves d'une sélection parallèle induite par une rupture des bouchons vaseux, reflétée par une divergence génomique répétée, avec des changements de fréquence alléliques parallèles à travers les estuaires. Nos résultats suggèrent que même l'isolement à court terme de de l'océan peut provoquer un changement dans le régime de sélection.</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>
					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/Guillaume_Belson1.jpg"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Guillaume BESLON</div>
							<div class="slide-date"><a href="https://liris.cnrs.fr"> LIRIS (Laboratoire d'InfoRmatique en Image et Systèmes d'information)</a></div>
							<div class="slide-title">Evolution expérimentale in silico avec la plate-forme Aevol</div>
							<div class="slide-text">S’il semble évident que les systèmes biologiques évoluent à l’échelle de leur séquence génétique et à l’échelle de leur phénotype ou de leur fitness, on oubli parfois qu’ils évoluent aussi « en tant que système », c’est-à-dire que les propriétés de ces systèmes (au premier titre d’entre elles, la taille du système, mais aussi sa modularité, sa robustesse, …) sont elles-aussi soumises à l’évolution. Si la biologie évolutive dispose d’un large corpus d’outils et de modèles pour étudier les deux premières échelles (typiquement la biologie moléculaire, la génétique des populations ou la génétique quantitative), l’étude de l’évolution des systèmes est plus récente et manque encore d’outils.
Dans la première partie de ce séminaire, je présenterai une famille d’outils, l’« évolution expérimentale in silico » (ou « génétique numérique ») qui permet d’étudier l’évolution de propriétés systémiques à différentes échelles, ainsi que la plate-forme d’évolution expérimentale in silico Aevol (www.aevol.fr), développée dans l’équipe Inria Beagle pour étudier l’évolution de la structure des génomes. Dans la deuxième partie du séminaire, je présenterai certains des résultats obtenus à l’aide de Aevol sur la dynamique des génomes et l’innovation évolutive.</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>

					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/frederic_delsuc.jpeg"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Fréderic DELSUC</div>
							<div class="slide-date"><a href="http://www.isem.univ-montp2.fr/fr/"> ISEM - CNRS</a></div>
							
							<div class="slide-title">Rubrique des chiens écrasés : assemblage hybride de génomes de mammifères de qualité à partir de spécimens récoltés le long des routes</div>
							<div class="slide-text">L'assemblage de génomes a été révolutionné depuis peu par l'arrivée des technologies permettant le séquençage direct de fragments d'ADN de plusieurs dizaines voire centaines de Kilobases. Cependant, le point faible des technologies de séquençage telles que PacBio et Oxford Nanopore demeure leur fort taux d'erreur dans la lecture des bases (10-15%). Cet inconvénient peut être compensé par un séquençage à forte couverture (50x) mais le coût devient alors prohibitif pour des génomes de grande taille comme ceux des mammifères qui dépassent pour la plupart 3 Gigabases. Ces limites ont encouragé le développement de méthodes d'assemblage hybride combinant des fragments courts de type Illumina avec très faible taux d'erreur pour corriger les séquences des longs fragments. Dans cette intervention, je présenterai nos récents efforts pour obtenir des génomes d'espèces de mammifères myrmécophages (qui mangent des fourmis et/ou termites) à partir d'échantillons issus d'animaux écrasés. Je monterai ainsi qu'une approche hybride combinant des fragments courts Illumina en forte couverture (80x) avec des fragments longs à faible couverture (10x) obtenus via le séquenceur portable MinION d’ Oxford Nanopore permet d'obtenir des génomes de qualité pour des espèces de mammifères difficiles d'accès et à un coût relativement raisonnable. Ces génomes de référence nous permettent d'étudier l'évolution moléculaire convergente liée à l'adoption du régime myrmécophage par plusieurs lignées indépendantes de mammifères.</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>

					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/Nicolas_Lartillot.jpg"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Nicolas LARTILLOT</div>
							<div class="slide-date"><a href="https://lbbe.univ-lyon1.fr/-A-propos-du-laboratoire-.html"> LBBE, Lyon</a></div>
							<div class="slide-title">Bayesian inference and probabilistic modeling in phylogenetics and macroevolution</div>
							<div class="slide-text">Over the last 20 years, Bayesian inference has revolutionized our practice in phylogenetics and molecular evolution. Thanks to a combination of complex hierarchical models, flexible priors, and generic Monte Carlo approaches, Bayesian inference offers a natural framework for implementing so-called integrative models of the intricate interplay between multiple levels of macroevolutionary processes (speciation, extinction, life-history and genetic sequence evolution). In this talk, several examples will be presented, illustrating how integrative modeling approaches can be used to address several current problems in evolutionary genomics: more accurately reconstructing deep phylogenies, getting a better understanding of the adaptive regimes followed by protein coding sequences, and making a link between phylogenetics, life-history evolution and population genetics.</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>

					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/christine_brun.png"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Christine BRUN</div>
							<div class="slide-date"><a href="https://tagc.univ-amu.fr/en">CNRS Research Director</a></div>
							
							<div class="slide-title">Yet another interaction network : 3'UTRs, lncRNAs and RNA-binding proteins
							</div>
							<div class="slide-text">Omics data are often represented as "biological networks". When abstracted and analyzed as graphs, these networks bring novel cellular mechanistic insights.<br>

							I will present our contributions to the understanding of post-transcriptional regulation, macromolecular complex assembly, and protein multifunctionality, through the integration of protein-protein and protein-RNA interaction networks, all issued from omics data. »</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>

					<div class="slide flex">
						<div class="slide-image slider-link next"><img src="./Images/orateurs_photos/aleksandra.jpg" alt="photo"><div class="overlay"></div></div>
						<div class="slide-content">
							<div class="slide-date name">Aleksandra PEKOWSKA</div>
							<div class="slide-date"><a href="https://tagc.univ-amu.fr/en">Nencki Institute of Experimental Biology</a></div>
							
							<div class="slide-title">Chromatin conformation assays - normalisation, feature extraction and comparative analysis
							</div>
							<div class="slide-text">Chromatin conformation assays such as Hi-C allowed us to gain new insights into the relationship between nuclear architecture and genome function. High-resolution Hi-C revealed the substructure of nuclear compartments, the partitioning of the genome into domains of preferential contacts termed topologically associating domains (TADs), and the existence of loops connecting CTCF sites. In my lecture, I will discuss Hi-C normalization methods, ways to identify compartments, TADs, and loops. I will also introduce a statistical framework of comparison of loop strengths across conditions.
							</div>
							<!-- <div class="slide-more">READ MORE</div> -->
						</div>	
					</div>

				</div>
				<div class="arrows">
					<a href="#" title="Previous" class="arrow slider-link prev"></a>
					<a href="#" title="Next" class="arrow slider-link next"></a>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js">
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<script type="text/javascript" src="js/gestion_anime.js"></script>

	<script src="js/TweenMax.min.js"></script>

	<script type="text/javascript">
		( function($) {

			$(document).ready(function() {

				var s       = $('.slider'),
				sWrapper    = s.find('.slider-wrapper'),
				sItem       = s.find('.slide'),
				btn         = s.find('.slider-link'),
				sWidth      = sItem.width(),
				sCount      = sItem.length,
				slide_date  = s.find('.slide-date'),
				slide_title = s.find('.slide-title'),
				slide_text  = s.find('.slide-text'),
				slide_more  = s.find('.slide-more'),
				slide_image = s.find('.slide-image img'),
				sTotalWidth = sCount * sWidth;

				sWrapper.css('width', sTotalWidth);
				sWrapper.css('width', sTotalWidth);

				var clickCount  = 0;

				btn.on('click', function(e) {
					e.preventDefault();

					if( $(this).hasClass('next') ) {

						( clickCount < ( sCount - 1 ) ) ? clickCount++ : clickCount = 0;
					} else if ( $(this).hasClass('prev') ) {

						( clickCount > 0 ) ? clickCount-- : ( clickCount = sCount - 1 );
					}
					TweenMax.to(sWrapper, 0.4, {x: '-' + ( sWidth * clickCount ) })


				  //CONTENT ANIMATIONS

				  var fromProperties = {autoAlpha:0, x:'-50', y:'-10'};
				  var toProperties = {autoAlpha:0.8, x:'0', y:'0'};

				  TweenLite.fromTo(slide_image, 1, {autoAlpha:0, y:'40'}, {autoAlpha:1, y:'0'});
				  TweenLite.fromTo(slide_date, 0.4, fromProperties, toProperties);
				  TweenLite.fromTo(slide_title, 0.6, fromProperties, toProperties);
				  TweenLite.fromTo(slide_text, 0.8, fromProperties, toProperties);
				  TweenLite.fromTo(slide_more, 1, fromProperties, toProperties);

				});

				document.addEventListener("keyup", function (event) {

					console.log("KeyboardEvent: key='" + event.key + "' | code='" + event.code + "'");
					if( $(".slider-link").hasClass('next') && event.key == "ArrowRight" ) {

						( clickCount < ( sCount - 1 ) ) ? clickCount++ : clickCount = 0;
					} else if ( $(".slider-link").hasClass('prev') && event.key == "ArrowLeft") {

						( clickCount > 0 ) ? clickCount-- : ( clickCount = sCount - 1 );
					}
					TweenMax.to(sWrapper, 0.4, {x: '-' + ( sWidth * clickCount ) })

				  //CONTENT ANIMATIONS

				  var fromProperties = {autoAlpha:0, x:'-50', y:'-10'};
				  var toProperties = {autoAlpha:0.8, x:'0', y:'0'};

				  TweenLite.fromTo(slide_image, 1, {autoAlpha:0, y:'40'}, {autoAlpha:1, y:'0'});
				  TweenLite.fromTo(slide_date, 0.4, fromProperties, toProperties);
				  TweenLite.fromTo(slide_title, 0.6, fromProperties, toProperties);
				  TweenLite.fromTo(slide_text, 0.8, fromProperties, toProperties);
				  TweenLite.fromTo(slide_more, 1, fromProperties, toProperties);
				});



			});
		})(jQuery);

		$('.overlay').addClass('overlay-blue');
	</script>

	<?php
	echo_footer();
	?>
