<?php
//on inclut le fichier contenant les fonctions php
require("../functions.php");

//on mets en place le header
echo_header("partenaires");
?>

<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link href="my_style.css" rel="stylesheet">

<h3 style="text-align:center;">Un grand merci à nos partenaires de toujours.</h3>

<div class="container">
    <section class="customer-logos slider">
        <div class="slide">
            <a href="http://www.umontpellier.fr/" target="_blank">
                <img src="Images/partenaires/LOGO_original_RVB_WEB-1.png" alt="Univérsité de Montpellier">
            </a>
        </div>

        <div class="slide">
            <a href="http://sciences.edu.umontpellier.fr/" target="_blank">
                <img src="Images/partenaires/fds.jpg" alt="Faculté des Sciences Montpellier">
            </a>
        </div>

        <div class="slide">
            <a href="http://medecine.edu.umontpellier.fr/" target="_blank">
                <img src="Images/partenaires/Medecine_logo_2015.jpg" alt="Faculté de Médecine Montpellier-Nimes">
            </a>
        </div>

        <div class="slide">
            <a href="http://informatique-fds.edu.umontpellier.fr/" target="_blank">
                <img src="Images/partenaires/image-15466.jpg" alt="Département d'informatique-FdS-UM2">
            </a>
        </div>

        <div class="slide">
            <a href="http://www.mathfds.univ-montp2.fr/" target="_blank">
                <img src="Images/partenaires/logo_math.png" alt="Département de Mathématiques">
            </a>
        </div>

        <div class="slide">
            <a href="http://www.mines-ales.fr/" target="_blank">
                <img src="Images/partenaires/mines_ales_1_new_logo.jpg" alt="IMT Mines d'Alès">
            </a>
        </div>

        <div class="slide">
            <a href="http://sns.edu.umontpellier.fr/master-sciences-numerique-pour-la-sante-montpellier/bcd/" target="_blank">
                <img src="Images/partenaires/Logo_BCD-1.png" alt="BCD">
            </a>
        </div>

        <div class="slide"> 
            <a href="http://astico.fr/" target="_blank">
                <img src="Images/partenaires/Logo_astico.png" alt="Astico">
            </a>
        </div>



    <!-- <h3 style="text-align:center;">Un grand merci à nos partenaires 2020.</h3> 

    <div id="liste_partenaire">
        <div id="partenaire">
            <a href="https://www.sfbi.fr/home" target="_blank"><img src="Images/partenaires/2-SFBI.png" alt="SFBI"></a>
        </div>
        <div id="partenaire">
            <a href="http://archives.cnrs.fr/ins2i/article/1497" target="_blank"><img src="Images/partenaires/logo_gdr_new.png" alt="GDR_BIM"></a>
        </div>
        <div id="partenaire">
            <a href="https://nanoporetech.com/" target="_blank"><img src="Images/partenaires/Nanopore.png" alt="Numev"></a>
        </div>
    </div> -->
   </section>

</div>

<h3 style="text-align:center;">Un grand merci à nos partenaires 2020.</h3> 

<div class="container">
    <section class="customer-logos slider">

        <div class="slide">
            <a href="https://www.sfbi.fr/home" target="_blank">
                <img src="Images/partenaires/2-SFBI.png" alt="SFBI">
            </a>
        </div>

        <div class="slide">
            <a href="http://www.labex-cemeb.org/fr" target="_blank">
                <img src="Images/partenaires/labex_cemed.jpg" alt="LABEX CeMed">
            </a>
        </div>

        <div class="slide"> 
            <a href="https://nanoporetech.com/" target="_blank">
                <img src="Images/partenaires/Nanopore.png" alt="Nanopore">
            </a>
        </div>

        <div class="slide"> 
            <a href="https://www.lirmm.fr/numev/" target="_blank">
                <img src="Images/partenaires/Numev.jpg" alt="Numev">
            </a>
        </div>

        <div class="slide"> 
            <a href="#" target="_blank">
                <img src="Images/partenaires/Logo_MUSE_Original.png" alt="Muse">
            </a>
        </div>

        <div class="slide"> 
            <a href="#" target="_blank">
                <img src="Images/partenaires/logo_ifb.jpg" alt="IFB">
            </a>
        </div>

        <div class="slide"> 
            <a href="#" target="_blank">
                <img src="Images/partenaires/logo_jebif_2.png" alt="jebif">
            </a>
        </div>

        <div class="slide"> 
            <a href="#" target="_blank">
                <img src="Images/partenaires/logo_region.jpg" alt="region">
            </a>
        </div>

        
   </section>

</div>

<div class="outer-div">
    <div class="inner-div">
        <span id="button_slide" class="glyphicon glyphicon-chevron-down" >  
        </span>
    </div>
</div>


<script type="text/javascript">
   $(document).ready(function(){
        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    });

    var visible_slide = false;

    $("#button_slide").click(function() {
        open_div();
        if (!visible_slide && !open) {

            $(".mask_container").addClass("visible_mask_container");
            $(".mask_container").removeClass("visible_mask_container_hide");
            $(".glyphicon").removeClass("glyphicon-chevron-down");
            $(".glyphicon").addClass("glyphicon-chevron-up");

            visible_slide = true;
        }
        else if(open){

            $(".mask_container").removeClass("visible_mask_container");
            $(".mask_container").addClass("visible_mask_container_hide");
            $(".glyphicon").addClass("glyphicon-chevron-down");
            $(".glyphicon").removeClass("glyphicon-chevron-up");

            visible_slide = false;
        }

    });

    var size = 0;
    var open = false;

    var myVar = null;

    function open_div(){
        if( myVar == null ){
            myVar = setInterval( div_head, 5 );
        }
    }

    function div_head(){
        
        $(".mask_container").css("height", size+"px");
        
        if( !open ){
            
            if( size <= 1000 ){
                size += 10;   
            }
            else{
                $(".mask_container").css("height", "auto");
                open = true;
                clearInterval(myVar);
                myVar = null;
            }
        }
        else{
            
            if( size > 0 ){
                size -= 10;
            }
            else{
                $(".mask_container").css("height", "0px");
                open = false;
                clearInterval(myVar);
                myVar = null;
            }
        }
    }

</script>


<div class="mask_container">

<div id="content_partenaire" class="scale-up-center">

    <h3 class="scale-up-center" style="text-align:center;">Un grand merci à nos partenaires de toujours.</h3>

    <div id="liste_partenaire">
        <div id="partenaire">
            <a href="http://www.umontpellier.fr/" target="_blank"><img src="Images/partenaires/LOGO_original_RVB_WEB-1.png" alt="Univérsité de Montpellier"></a>
        </div>
        <div id="partenaire">
            <a href="http://sciences.edu.umontpellier.fr/" target="_blank"><img src="Images/partenaires/fds.jpg" alt="Faculté des Sciences Montpellier"></a>
        </div>
        <div id="partenaire">
            <a href="http://medecine.edu.umontpellier.fr/" target="_blank"><img src="Images/partenaires/Medecine_logo_2015.jpg" alt="Faculté de Médecine Montpellier-Nimes"></a>
        </div>
    </div>

    <div id="liste_partenaire">
        <div id="partenaire">
            <a href="http://informatique-fds.edu.umontpellier.fr/" target="_blank"><img src="Images/partenaires/image-15466.jpg" alt="Département d'informatique-FdS-UM2"></a>
        </div>
        <div id="partenaire">
            <a href="http://www.mathfds.univ-montp2.fr/" target="_blank"><img src="Images/partenaires/logo_math.png" alt="Département de Mathématiques"></a>
        </div>
        <div id="partenaire">
            <a href="http://www.mines-ales.fr/" target="_blank"><img src="Images/partenaires/mines_ales_1_new_logo.jpg" alt="IMT Mines d'Alès"></a>
        </div>
    </div>

    <div id="liste_partenaire">
        <div id="partenaire">
            <a href="http://sns.edu.umontpellier.fr/master-sciences-numerique-pour-la-sante-montpellier/bcd/" target="_blank"><img src="Images/partenaires/Logo_BCD-1.png" alt="Département d'informatique-FdS-UM2"></a>
        </div>
        <div id="partenaire">
            <a href="http://astico.fr/" target="_blank"><img src="Images/partenaires/Logo_astico.png" alt="Département de Mathématiques"></a>
        </div>
    </div>

    <div class="line"></div>

    <h3 style="text-align:center;">Un grand merci à nos partenaires 2020.</h3>

    <div id="liste_partenaire">
        <div id="partenaire">
            <a href="https://www.sfbi.fr/home" target="_blank"><img src="Images/partenaires/2-SFBI.png" alt="SFBI"></a>
        </div>
        <div id="partenaire">
            <a href="http://www.labex-cemeb.org/fr" target="_blank">
                <img src="Images/partenaires/labex_cemed.jpg" alt="LABEX CeMed">
            </a>
        </div>
        <div id="partenaire">
            <a href="https://nanoporetech.com/" target="_blank"><img src="Images/partenaires/Nanopore.png" alt="Numev"></a>
        </div>

        <div id="partenaire">
        <a href="https://www.lirmm.fr/numev/" target="_blank">
            <img src="Images/partenaires/Numev.jpg" alt="Numev">
        </a>
        </div>

        <div id="partenaire" class="muse"> 
        <a href="#" target="_blank">
            <img src="Images/partenaires/Logo_MUSE_Original.png" alt="Muse">
        </a>
    </div>

    
    </div>

    <div id="liste_partenaire">
    <div id="partenaire"> 
        <a href="#" target="_blank">
            <img src="Images/partenaires/logo_ifb.jpg" alt="IFB">
        </a>
    </div>

    <div id="partenaire"> 
        <a href="#" target="_blank">
            <img src="Images/partenaires/logo_jebif_2.png" alt="IFB">
        </a>
    </div>

    <div id="partenaire"> 
        <a href="#" target="_blank">
            <img src="Images/partenaires/logo_region.jpg" alt="IFB">
        </a>
    </div>
</div>


    
    
    <!-- <div id="liste_partenaire">
        <div id="partenaire">
            <a href="http://www.labex-cemeb.org" target="_blank"><img src="Images/partenaires/7-CEMEB.png" alt="Cemeb"></a>
        </div>
        <div id="partenaire">
            <a href="http://www.cnrs.fr/fr/page-daccueil" target="_blank"><img src="Images/partenaires/CNRS.png" alt="CNRS"></a>
        </div>
		<div id="partenaire">
            <a href="https://languedoc-roussillon-universites.fr" target="_blank"><img src="Images/partenaires/lruniversite.jpg" alt="Languedoc Roussillon Universite"></a>
        </div>

    </div>
        <div id="liste_partenaire">
        
        <div id="partenaire">
            <a href="https://www.laregion.fr" target="_blank"><img src="Images/partenaires/region_occitanie.png" alt="Région occitannie"></a>
        </div>
        <div id="partenaire">
            <a href="https://https://nanoporetech.com" target="_blank"><img src="Images/partenaires/Nanopore.png" alt="Oxford Nanopore"></a>
        </div>
		

    </div> -->
</div>
</div>

<?php
echo_footer();
?>
