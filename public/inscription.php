<?php
//on inclut le fichier contenant les fonctions php
require("../functions.php");
check_date();
//on mets en place le header
echo_header("inscription");
function checklimit($id) {
    $bdd = make_db_connection();
    $req = "SELECT COUNT(*) FROM Participe WHERE id_evenement = ".$id.";";
    $result = $bdd->query($req);
    $data = $result->fetchAll();
    if($data[0][0] >= 36){
        return false;
    }else{
        return true;
    }
};
if(isset($_GET['err']))
{
    $errors = [
        1 => 'Vous êtes déja inscrit avec cette adresse mail.',
        2 => 'Un ou plusieurs champs sont mal remplis',
        3 => 'Le captcha n\'a pas été correctement validé',
        4 => 'Vous n\'avez pas choisi de workshop',
        5 => 'Vous n\'avez pas rempli l\'abstract',
        6 => 'Vous ne pouvez pas sélectionner les deux workshops'
    ];
    ?>
    <div class="alert alert-danger fade in">
        <a href="#" data-dismiss="alert">&times;</a>
        <strong>Erreur!</strong> <?php echo $errors[$_GET['err']]; ?>
    </div>
    <?php
}
?>

<noscript>
    <div class="alert alert-danger fade in">
        <a href="#" data-dismiss="alert">&times;</a>
        <strong>Veuillez activer javascript!</strong>
    </div>
</noscript>

<div id="content">
    <div id="formulaire" class="border-0">
        <h2 style="margin-top: 0">Inscriptions</h2>
        <h2 style="margin-top: 10px;"><small>(gratuites, mais obligatoire pour des raisons de logistique)</small></h2>

        <form role="form" action="inscription_serveur.php" method="post" id="form" name="form">
            <div class="message message-danger">
                    <!-- <p>L'inscription n'est pas encore ouverte !<br>
                    Cependant, vous pouvez toujours vous inscrire pour les conférences, et cela jusqu'au dernier moment.</p> -->
                    <p>Les inscriptions aux formations ne sont plus possibles.<br>Les inscriptions aux conférences fermeront le mercredi 05 février</p>
            </div> 
            <div class="message message-info">
                <p>Les inscriptions sont ouvertes</p>
            </div>

            <h3>Informations générales</h3>

            <div class="form-group">
                <label for="civilite" class="control-label">Civilité</label>
                <select id="civilite" class="form-control" name="civilite">
                    <option value="" disabled selected>Civilité</option>
                    <option value="Monsieur">Monsieur</option>
                    <option value="Madame">Madame</option>
                </select>
            </div>

            <div class="form-group">
                <label for="statut" class="control-label">Statut</label>
                <select class="form-control" id="statut" placeholder="Statut*" name="statut" spellcheck="true">
                    <option value="" disabled selected>Statut</option>
                    <option value="etudiant">Étudiant</option>
                    <option value="docteur">Doctorant</option>
                    <option value="post-doctorant">Post-doctorant</option>
                    <option value="chercheur">Chercheur</option>
                    <option value="enseignant">Enseignant</option>
                    <option value="enseignant_chercheur">Enseignant chercheur</option>
                    <option value="prive">Privé</option>
                    <option value="ingenieur">Ingénieur</option>
                    <option value="autre">Autre</option>
                </select>
            </div>

            <div class="form-group">
                <label for="pays" class="control-label">Pays</label>
                <select class="form-control" id="pays" name="pays" placeholder="Pays*">
                    <option value="Canada" >Canada
                    <option value="Belgique" >Belgique
                    <option value="France"  selected="selected">France

                    <option value="Afghanistan" >Afghanistan
                    <option value="Afrique_Centrale" >Afrique_Centrale
                    <option value="Afrique_du_sud" >Afrique_du_Sud
                    <option value="Albanie" >Albanie
                    <option value="Algerie" >Algerie
                    <option value="Allemagne" >Allemagne
                    <option value="Andorre" >Andorre
                    <option value="Angola" >Angola
                    <option value="Anguilla" >Anguilla
                    <option value="Arabie_Saoudite" >Arabie_Saoudite
                    <option value="Argentine" >Argentine
                    <option value="Armenie" >Armenie
                    <option value="Australie" >Australie
                    <option value="Autriche" >Autriche
                    <option value="Azerbaidjan" >Azerbaidjan
                    <option value="Bahamas" >Bahamas
                    <option value="Bangladesh" >Bangladesh
                    <option value="Barbade" >Barbade
                    <option value="Bahrein" >Bahrein
                    <option value="Belgique" >Belgique
                    <option value="Belize" >Belize
                    <option value="Benin" >Benin
                    <option value="Bermudes" >Bermudes
                    <option value="Bielorussie" >Bielorussie
                    <option value="Bolivie" >Bolivie
                    <option value="Botswana" >Botswana
                    <option value="Bhoutan" >Bhoutan
                    <option value="Boznie_Herzegovine" >Boznie_Herzegovine
                    <option value="Bresil" >Bresil
                    <option value="Brunei" >Brunei
                    <option value="Bulgarie" >Bulgarie
                    <option value="Burkina_Faso" >Burkina_Faso
                    <option value="Burundi" >Burundi
                    <option value="Caiman" >Caiman
                    <option value="Cambodge" >Cambodge
                    <option value="Cameroun" >Cameroun
                    <option value="Canaries" >Canaries
                    <option value="Cap_vert" >Cap_Vert
                    <option value="Chili" >Chili
                    <option value="Chine" >Chine
                    <option value="Chypre" >Chypre
                    <option value="Colombie" >Colombie
                    <option value="Comores" >Colombie
                    <option value="Congo" >Congo
                    <option value="Congo_democratique" >Congo_democratique
                    <option value="Cook" >Cook
                    <option value="Coree_du_Nord" >Coree_du_Nord
                    <option value="Coree_du_Sud" >Coree_du_Sud
                    <option value="Costa_Rica" >Costa_Rica
                    <option value="Cote_d_Ivoire" >Côte_d_Ivoire
                    <option value="Croatie" >Croatie
                    <option value="Cuba" >Cuba
                    <option value="Danemark" >Danemark
                    <option value="Djibouti" >Djibouti
                    <option value="Dominique" >Dominique
                    <option value="Egypte" >Egypte
                    <option value="Emirats_Arabes_Unis" >Emirats_Arabes_Unis
                    <option value="Equateur" >Equateur
                    <option value="Erythree" >Erythree
                    <option value="Espagne" >Espagne
                    <option value="Estonie" >Estonie
                    <option value="Etats_Unis" >Etats_Unis
                    <option value="Ethiopie" >Ethiopie
                    <option value="Falkland" >Falkland
                    <option value="Feroe" >Feroe
                    <option value="Fidji" >Fidji
                    <option value="Finlande" >Finlande
                    <option value="Gabon" >Gabon
                    <option value="Gambie" >Gambie
                    <option value="Georgie" >Georgie
                    <option value="Ghana" >Ghana
                    <option value="Gibraltar" >Gibraltar
                    <option value="Grece" >Grece
                    <option value="Grenade" >Grenade
                    <option value="Groenland" >Groenland
                    <option value="Guadeloupe" >Guadeloupe
                    <option value="Guam" >Guam
                    <option value="Guatemala" >Guatemala
                    <option value="Guernesey" >Guernesey
                    <option value="Guinee" >Guinee
                    <option value="Guinee_Bissau" >Guinee_Bissau
                    <option value="Guinee_equatoriale" >Guinee_Equatoriale
                    <option value="Guyana" >Guyana
                    <option value="Guyane_Francaise" >Guyane_Francaise
                    <option value="Haiti" >Haiti
                    <option value="Hawaii" >Hawaii
                    <option value="Honduras" >Honduras
                    <option value="Hong_Kong" >Hong_Kong
                    <option value="Hongrie" >Hongrie
                    <option value="Inde" >Inde
                    <option value="Indonesie" >Indonesie
                    <option value="Iran" >Iran
                    <option value="Iraq" >Iraq
                    <option value="Irlande" >Irlande
                    <option value="Islande" >Islande
                    <option value="Israel" >Israel
                    <option value="Italie" >italie
                    <option value="Jamaique" >Jamaique
                    <option value="Jan Mayen" >Jan Mayen
                    <option value="Japon" >Japon
                    <option value="Jersey" >Jersey
                    <option value="Jordanie" >Jordanie
                    <option value="Kazakhstan" >Kazakhstan
                    <option value="Kenya" >Kenya
                    <option value="Kirghizstan" >Kirghizistan
                    <option value="Kiribati" >Kiribati
                    <option value="Koweit" >Koweit
                    <option value="Laos" >Laos
                    <option value="Lesotho" >Lesotho
                    <option value="Lettonie" >Lettonie
                    <option value="Liban" >Liban
                    <option value="Liberia" >Liberia
                    <option value="Liechtenstein" >Liechtenstein
                    <option value="Lituanie" >Lituanie
                    <option value="Luxembourg" >Luxembourg
                    <option value="Lybie" >Lybie
                    <option value="Macao" >Macao
                    <option value="Macedoine" >Macedoine
                    <option value="Madagascar" >Madagascar
                    <option value="Madère" >Madère
                    <option value="Malaisie" >Malaisie
                    <option value="Malawi" >Malawi
                    <option value="Maldives" >Maldives
                    <option value="Mali" >Mali
                    <option value="Malte" >Malte
                    <option value="Man" >Man
                    <option value="Mariannes_du_Nord" >Mariannes du Nord
                    <option value="Maroc" >Maroc
                    <option value="Marshall" >Marshall
                    <option value="Martinique" >Martinique
                    <option value="Maurice" >Maurice
                    <option value="Mauritanie" >Mauritanie
                    <option value="Mayotte" >Mayotte
                    <option value="Mexique" >Mexique
                    <option value="Micronesie" >Micronesie
                    <option value="Midway" >Midway
                    <option value="Moldavie" >Moldavie
                    <option value="Monaco" >Monaco
                    <option value="Mongolie" >Mongolie
                    <option value="Montserrat" >Montserrat
                    <option value="Mozambique" >Mozambique
                    <option value="Namibie" >Namibie
                    <option value="Nauru" >Nauru
                    <option value="Nepal" >Nepal
                    <option value="Nicaragua" >Nicaragua
                    <option value="Niger" >Niger
                    <option value="Nigeria" >Nigeria
                    <option value="Niue" >Niue
                    <option value="Norfolk" >Norfolk
                    <option value="Norvege" >Norvege
                    <option value="Nouvelle_Caledonie" >Nouvelle_Caledonie
                    <option value="Nouvelle_Zelande" >Nouvelle_Zelande
                    <option value="Oman" >Oman
                    <option value="Ouganda" >Ouganda
                    <option value="Ouzbekistan" >Ouzbekistan
                    <option value="Pakistan" >Pakistan
                    <option value="Palau" >Palau
                    <option value="Palestine" >Palestine
                    <option value="Panama" >Panama
                    <option value="Papouasie_Nouvelle_Guinee" >Papouasie_Nouvelle_Guinee
                    <option value="Paraguay" >Paraguay
                    <option value="Pays_Bas" >Pays_Bas
                    <option value="Perou" >Perou
                    <option value="Philippines" >Philippines
                    <option value="Pologne" >Pologne
                    <option value="Polynesie" >Polynesie
                    <option value="Porto_Rico" >Porto_Rico
                    <option value="Portugal" >Portugal
                    <option value="Qatar" >Qatar
                    <option value="Republique_Dominicaine" >Republique_Dominicaine
                    <option value="Republique_Tcheque" >Republique_Tcheque
                    <option value="Reunion" >Reunion
                    <option value="Roumanie" >Roumanie
                    <option value="Royaume_Uni" >Royaume_Uni
                    <option value="Russie" >Russie
                    <option value="Rwanda" >Rwanda
                    <option value="Sahara_Occidental" >Sahara Occidental
                    <option value="Sainte_Lucie" >Sainte_Lucie
                    <option value="Saint_Marin" >Saint_Marin
                    <option value="Salomon" >Salomon
                    <option value="Salvador" >Salvador
                    <option value="Samoa_Occidentales" >Samoa_Occidentales
                    <option value="Samoa_Americaine" >Samoa_Americaine
                    <option value="Sao_Tome_et_Principe" >Sao_Tome_et_Principe
                    <option value="Senegal" >Senegal
                    <option value="Seychelles" >Seychelles
                    <option value="Sierra_Leone" >Sierra Leone
                    <option value="Singapour" >Singapour
                    <option value="Slovaquie" >Slovaquie
                    <option value="Slovenie" >Slovenie
                    <option value="Somalie" >Somalie
                    <option value="Soudan" >Soudan
                    <option value="Sri_Lanka" >Sri_Lanka
                    <option value="Suede" >Suede
                    <option value="Suisse" >Suisse
                    <option value="Surinam" >Surinam
                    <option value="Swaziland" >Swaziland
                    <option value="Syrie" >Syrie
                    <option value="Tadjikistan" >Tadjikistan
                    <option value="Taiwan" >Taiwan
                    <option value="Tonga" >Tonga
                    <option value="Tanzanie" >Tanzanie
                    <option value="Tchad" >Tchad
                    <option value="Thailande" >Thailande
                    <option value="Tibet" >Tibet
                    <option value="Timor_Oriental" >Timor_Oriental
                    <option value="Togo" >Togo
                    <option value="Trinite_et_Tobago" >Trinite_et_Tobago
                    <option value="Tristan_da_cunha" >Tristan de cuncha
                    <option value="Tunisie" >Tunisie
                    <option value="Turkmenistan" >Turmenistan
                    <option value="Turquie" >Turquie
                    <option value="Ukraine" >Ukraine
                    <option value="Uruguay" >Uruguay
                    <option value="Vanuatu" >Vanuatu
                    <option value="Vatican" >Vatican
                    <option value="Venezuela" >Venezuela
                    <option value="Vierges_Americaines" >Vierges_Americaines
                    <option value="Vierges_Britanniques" >Vierges_Britanniques
                    <option value="Vietnam" >Vietnam
                    <option value="Wake" >Wake
                    <option value="Wallis_et_Futuma" >Wallis et Futuma
                    <option value="Yemen" >Yemen
                    <option value="Yougoslavie" >Yougoslavie
                    <option value="Zambie" >Zambie
                    <option value="Zimbabwe" >Zimbabwe
                </select>
            </div>

            <div class="row">
                <span>
                    <input class="basic-slide" id="nom" type="text" placeholder="Nom *" name="nom"/>
                    <label for="nom">Nom</label>
                </span>
                <span>
                    <input class="basic-slide" id="prenom" type="text" placeholder="Prénom *" name="prenom"/>
                    <label for="prenom">Prénom</label>
                </span>
                <span>
                    <input class="basic-slide" id="mail" type="text" placeholder="Votre mail favoris *" name="mail"/>
                    <label for="mail">Adresse e-mail</label>
                </span>
                <span>
                    <input class="basic-slide" id="affiliation" type="text" placeholder="Composante/Laboratoire" name="affiliation" />
                    <label for="affiliation">Affiliation</label>
                </span>
            </div>
            <div id="error-event" class="message message-danger" style="display:none;">.</div>
            <!-- Ancien formulaire -->

            <!-- <div class="form-group">
                <label for="nom" class="control-label">Nom</label>
                <input id="nom" type="text" placeholder="Nom *" class="form-control" name="nom">
            </div>

            <div class="form-group">
                <label for="prenom" class="control-label">Prénom</label>
                <input id="prenom" type="text" placeholder="Prénom *" class="form-control" name="prenom">
            </div>

            <div class="form-group">
                <label for="mail" class="control-label">Adresse e-mail</label>
                <input type="text" class="form-control" placeholder="Mail *" id="mail"
                aria-describedby="inputGroupSuccess1Status" name="mail">
            </div> -->
            
            <h3>Journées auxquelles vous souhaitez participer</h3>
            
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cb_ws" id="cb_ws" value="on" disabled="disabled">
                    <p class="checkbox-label-text">6 Février - Formations</p>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cb_c" id="cb_c" value="on">
                    <p class="checkbox-label-text">7 Février - Conférences</p>
                </label>
            </div>
            <div id="radio-workshop">
                <h3>Formations auxquelles vous voulez assister <small>le Jeudi 6 Février</small></h3>
                <div class="message message-info">
                    <p>Vous pouvez vous inscrire à un ou deux workshop (matin et/ou aprés midi).</p>
                </div>
                <div class="message message-danger">
                    <p>Les Formations Python/Bash et Alignement sont désormais complets.<br>
                    Afin de garantir que les workshops se déroulent dans de bonnes conditions pour tout le monde, nous ne pouvons
                    pas accepter de nouvelles inscriptions.</p>
                </div> 
                <div id="error-workshop" class="message message-danger" style="display:none">
                    Merci de sélectionner le workshop par lequel vous êtes intéressé.
                </div>
                <label>Matin</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="option1" id="optionR_AM" value="R" >
                        <p class="checkbox-label-text">Introduction aux langages de programmation (R)</p>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="option1" id="optionPython" value="Python" disabled="disabled" >
                        <p class="checkbox-label-text">Introduction aux langages de programmation (Python/Bash)</p>
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="option1" id="optionRequetage" value="Requetage" >
                        <p class="checkbox-label-text">Introduction au langage SQL (base de données)</p>
                    </label>
                </div>

                <!-- <div class="radio">
                    <label>
                        <input type="radio" name="option1" id="optionBASH" value="BASH" onchange="changeWS('optionBASH');" <?php //if(!checklimit(4)){echo 'disabled';}?>>
                        <p class="checkbox-label-text"> Lignes de commandes Bash </p>
                    </label>
                </div> -->
                <br/>
                <label>Après-midi</label>
                <!-- <div class="radio">
                    <label>
                        <input type="radio" name="option2" id="optionR_PM" value="R" >
                        <p class="checkbox-label-text">Manipulation de données statistiques via R</p>
                        <p style="margin-left:4px;"><small>(Des prérequis en statistiques sont demandés pour suivre ce workshop ou avoir suivi l'introduction à R le matin)</small></p>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="option2" id="optionPYTHON" value="PYTHON" >
                        <p class="checkbox-label-text"> Introduction au langage Python </p>
                    </label>
                </div> -->

                <div class="radio">
                    <label>
                        <input type="radio" name="option2" id="optionPhy" value="Phylogenie" >
                        <p class="checkbox-label-text"> Phylogénie </p>
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="option2" id="optionAA" value="Alignement_Assemblage" disabled="disabled">
                        <p class="checkbox-label-text"> Alignement/Assemblage </p>
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="option2" id="optionML" value="Machine_Learning" >
                        <p class="checkbox-label-text"> Introduction au machine learning </p>
                    </label>
                </div>
            </div>
            <br/>
            <?php /*<h3>Soumission d'abstract <small>Optionnel</small></h3>
            <div class="message message-info">
                <p>Vous avez la possibilité de soumettre un abstract décrivant vos travaux en cochant la case ci dessous.
                    <br/>
                Notre comité séléctionnera les abstracts et vous pourrez éventuellement présenter votre projet
                le jour des conférences, soit en tant que conférencier, soit pour la présentation de poster.
                </p>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cb_a" id="cb_a" value="on" onChange="displayAbstract();">
                    <p class="checkbox-label-text">Soumettre un abstract</p>
                </label>
            </div>
            <br/>
            <div class="form-group">
                <textarea id="file" class="form-control" type="text" id="exampleInputFile" maxlength=2000
                placeholder="Tapez le résumé de votre présentation en maximum 2000 mots"
                rows="10" name="file"></textarea>
            </div>*/?>

            <h3>Droit à l'image <small>Optionnel</small></h3>
            <div class="message message-info">
                <p>
                    Des photos et des vidéos pourront être capturées pendant l'évènement.<br>
                    En cochant cette case, vous nous permettez de publier des photos et vidéos vous représentant sur notre site et nos réseaux sociaux.
                </p>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="droit_image">
                    <p class="checkbox-label-text">Accorder les droits à l'image</p>
                </label>
            </div>
            <br/>
            <div class="g-recaptcha" data-sitekey="<?php echo $RECAPTCHA_KEY; ?>"></div>
            <br/>

            <button type="submit" class="btn btn-primary" required id="btn_submit">
                <i class="icon icon-check icon-lg"></i> Valider
            </button>

        </form>
    </div>
</div>


<?php
echo_footer();
?>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<noscript>
  <div>
    <div style="width: 302px; height: 422px; position: relative;">
      <div style="width: 302px; height: 422px; position: absolute;">
        <iframe src="https://www.google.com/recaptcha/api/fallback?k=your_site_key"
                frameborder="0" scrolling="no"
                style="width: 302px; height:422px; border-style: none;">
        </iframe>
      </div>
    </div>
    <div style="width: 300px; height: 60px; border-style: none;
                   bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
                   background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
      <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                   class="g-recaptcha-response"
                   style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                          margin: 10px 25px; padding: 0px; resize: none;" >
      </textarea>
    </div>
  </div>
</noscript>

<script>
    var buton = document.getElementById('btn_submit');
    var civilite = document.getElementById('civilite');
    var nom = document.getElementById('nom');
    var prenom = document.getElementById('prenom');
    var mail = document.getElementById('mail');
    var statut = document.getElementById('statut');
    // var file = document.getElementById('file');
    //on change le type du boutton pour ne pas être redirigé si js est activé
    buton.type="button";
    assistWorkshop();
    //displayAbstract();
    function initForm(element)
    {
        element.className="form-control";
    }

    function assistWorkshop()
    {
        ws = document.getElementById('radio-workshop');
        if(document.getElementById('cb_ws').checked)
        {
            ws.style.display="block";
        }
        else
        {
            ws.style.display="none";
        }
    }
    /* function displayAbstract()
    {
        if(document.getElementById('cb_a').checked)
        {
            document.getElementById('file').style.display="block";
        }
        else
        {
           document.getElementById('file').style.display="none";
        }
    }*/
    civilite.addEventListener('focus',function()
    {
        initForm(civilite);
    });
    nom.addEventListener('focus',function()
    {
        //initForm(nom);
    });
    prenom.addEventListener('focus',function()
    {
        //initForm(prenom);
    });
    mail.addEventListener('focus',function()
    {
        //initForm(mail);
    });
    statut.addEventListener('focus',function()
    {
        //initForm(statut);
    });
    /* file.addEventListener('focus',function()
    {
        initForm(file);
    });*/
    function hideCheckboxErrorMessage(id) {
        document.getElementById(id).style.display = "none";
    }
    function changeWS(id)
    {
        rd1 = document.getElementById(id);
        /*if(id=="optionR_AM")
        {
            document.getElementById("optionBASH").checked=false;
        }
        if(id=="optionBASH")
        {
            document.getElementById("optionR_AM").checked=false;
        }
        if(id=="optionR_PM")
        {
            document.getElementById("optionPYTHON").checked=false;
        }
        if(id=="optionPYTHON")
        {
            document.getElementById("optionR_PM").checked=false;
        }*/
    }
    document.getElementById('cb_ws').addEventListener('change', function ()
    {
        hideCheckboxErrorMessage('error-event');
        assistWorkshop();
    });
    document.getElementById('cb_c').addEventListener('change', function ()
    {
        hideCheckboxErrorMessage('error-event');
    });
    /*document.getElementById("optionR_AM").addEventListener('change',function()
    {
        hideCheckboxErrorMessage('error-workshop');
    });
    document.getElementById("optionR_PM").addEventListener('change',function()
    {
        hideCheckboxErrorMessage('error-workshop');
    });
    document.getElementById("optionBASH").addEventListener('focus',function()
    {
        hideCheckboxErrorMessage('error-workshop');
    });
    document.getElementById("optionPYTHON").addEventListener('focus',function()
    {
        hideCheckboxErrorMessage('error-workshop');
    });*/
    //événement sur le bouton de submission
    buton.addEventListener('click',function()
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var is_erreur = 0;
        if(nom.value=="")
        {
            nom.className="champ_erreur form-control";
            is_erreur = 1;
        }
        if(prenom.value=="")
        {
            prenom.className="champ_erreur form-control";
            is_erreur = 1;
        }
        if(!re.test(mail.value))
        {
            mail.className="champ_erreur form-control";
            is_erreur = 1;
        }
        /* if(document.getElementById('cb_a').checked & file.value=="")
        {
            file.className="champ_erreur form-control";
            is_erreur = 1;
        }*/
        /*if(document.getElementById('cb_ws').checked & !document.getElementById('optionR_AM').checked
            & !document.getElementById('optionBASH').checked
            & !document.getElementById('optionR_PM').checked
            & !document.getElementById('optionPYTHON').checked)
        {
            document.getElementById('error-workshop').style.display = "block";
            is_erreur = 1;
        }*/
        /*if(!document.getElementById('cb_ws').checked & !document.getElementById('cb_c').checked)
        {
            document.getElementById('error-event').style.display = "block";
            is_erreur = 1;
        }*/
        if(!is_erreur)
        {
            //buton.form.submit();
            //document.form.submit();
            form = document.getElementById('form');
            form.submit();
        }
    });

</script>
