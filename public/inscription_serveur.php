<?php

//on inclut le fichier contenant les fonctions php
require("../functions.php");

check_date();

//on mets en place le header
echo_header("inscription");


//ouverture base de donnée
$bdd = make_db_connection();

if(isset($_POST['nom']) and isset($_POST['prenom']) 
	and isset($_POST['mail']) and ( isset($_POST['cb_ws']) or isset($_POST['cb_c']) ) 
	and isset($_POST['g-recaptcha-response']) )
{
	if(isset($_POST['cb_ws']) & !isset($_POST['option1']) & !isset($_POST['option2']))
	{
		header('Location:inscription.php?err=4');
	}
	else
	{
		if(isset($_POST['cb_a']) & (!isset($_POST['file']) or $_POST['file']==""))
		{
			header('Location:inscription.php?err=5');
		}
		else
		{
			//sécuriser les données entrantes
			$civilite =(isset($_POST['civilite'])) ? $_POST['civilite'] : "";
			$nom = $_POST['nom'];
			$prenom = $_POST['prenom'];
			$mail = $_POST['mail'];
			$statut = (isset($_POST['statut'])) ? $_POST['statut'] : "";
			$abstract = (isset($_POST['cb_a']) and $_POST['cb_a']="on" and isset($_POST['file'])) ? $_POST['file'] : "";
			$droit_image = (isset($_POST['droit_image']) and (($_POST['droit_image'])=="on")) ? 1 : 0;
			$pays = $_POST['pays'];
			$affiliation = $_POST['affiliation'];
			$array_evenement = array(
				(isset($_POST['cb_c']) and $_POST['cb_c']=="on")?1:0,
				(isset($_POST['cb_ws']) and $_POST['cb_ws']=="on" and isset($_POST['option1']) and $_POST['option1']=="R")?1:0,
				(isset($_POST['cb_ws']) and $_POST['cb_ws']=="on" and isset($_POST['option1']) and $_POST['option1']=="Python")?1:0,
				(isset($_POST['cb_ws']) and $_POST['cb_ws']=="on" and isset($_POST['option1']) and $_POST['option1']=="Requetage")?1:0,
				(isset($_POST['cb_ws']) and $_POST['cb_ws']=="on" and isset($_POST['option2']) and $_POST['option2']=="Phylogenie")?1:0,
				(isset($_POST['cb_ws']) and $_POST['cb_ws']=="on" and isset($_POST['option2']) and $_POST['option2']=="Alignement_Assemblage")?1:0,
				(isset($_POST['cb_ws']) and $_POST['cb_ws']=="on" and isset($_POST['option2']) and $_POST['option2']=="Machine_Learning")?1:0);
			
			$rep_captcha = $_POST['g-recaptcha-response'];
			$remoteip = $_SERVER['REMOTE_ADDR'];

			$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=" 
			. $RECAPTCHA_SECRET
			. "&response=" . $rep_captcha
			. "&remoteip=" . $remoteip ;

			$decode = json_decode(file_get_contents($api_url), true);

			if ($decode['success'] != true) 
			{
				// C'est un robot
				//retourner une erreur
				header('Location:inscription.php?err=3');
			}
			
			/*if( ($array_evenement[1] == 1 and $array_evenement[2] == 1) 
				or ($array_evenement[2] == 1 and $array_evenement[4] == 1)){
				// Deux workshops au même moment de la journée sont choisis, c'est pas possible
				header('Location:inscription.php?err=6');
			}*/

			//sécuriser les requêtes
			$requete_mail = "SELECT mail FROM Inscrit WHERE mail=:mail";
			$requete_mail = $bdd->prepare($requete_mail);
			$requete_mail->execute(['mail' => $mail]);

			$requete_soumission = "INSERT INTO Inscrit(mail, civilite, nom, prenom, statut, abstract, droit_image, pays, affiliation)
									VALUES(:mail,:civilite,:nom,:prenom,:statut,:abstract,:droit_image,:pays,:affiliation)";

			//vérifier que l'adresse mail 'est pas déja dans la bdd
			if($requete_mail->fetch())
			{
				//retourner une erreur
				header('Location:inscription.php?err=1');
			}
				
			//vérifier le contenu des données
			if($nom=="" or $prenom=="" or $mail=="")
			{
				header('Location:inscription.php?err=2');
			}

			$requete_soumission = $bdd->prepare($requete_soumission);
			$array_soumission = array(
			'mail' => $mail,
			'civilite' => $civilite,
			'nom' => $nom,
			'prenom' => $prenom,
			'statut' => $statut,
			'abstract' => $abstract,
			'droit_image' => $droit_image,
			'pays' => $pays,
			'affiliation' => $affiliation
			);
			$requete_soumission->execute($array_soumission);

			//$arr = $requete_soumission->errorInfo();
			//print_r($arr);

			//Liaison des événements choisis avec l'inscrit dans la table "participe"
			for($i=0; $i<sizeof($array_evenement); $i++)
			{
				if($array_evenement[$i] == 1)
				{
					$requete_soumission = "INSERT INTO Participe(mail_inscrit,id_evenement)
									VALUES(:mail,:evenement)";
					$requete_soumission = $bdd->prepare($requete_soumission);
					$array_soumission = array(
					'mail' => $mail,
					'evenement' => $i+1
					);
					$requete_soumission->execute($array_soumission);
				}	
			}
			


			echo '<div class="alert alert-success">
				<strong>Félicitations !</strong> Vous avez été enregsitré avec succès.<br/>
				Vous allez recevoir un mail de confirmation.
				Vous allez être redirigé sur la page d\'accueil...
				Si vous n\'êtes pas redirigé dans les 3 secondes sur la page d\'accueil, cliquez <a href="index.php">ici</a>. 
			</div>';
			// Redirection bougée dans le try pour pas rediriger si le mail s'est pas envoyé

			/*
			 * Envoi du mail
			 * NOTE : si MAIL_SEND est a false dans vars.php l'envoi sera 
			 * ignoré silencieusement
			 */

			// On charge le template
			$message = file_get_contents('../mails/inscription.html');

			// On le personnalise en remplaçant les champs
			$personalisation = [
				"nom" => $nom,
				"prenom" => $prenom,
				"sessions" => ''
			];

			// On génère directement la liste des sessions sur place
			//option1
			if ($array_evenement[1] == 1){
				$personalisation['sessions'] .= '<strong>Workshop "Introduction aux langages de programmation (R)"</strong>, le matin du jeudi 6 février<br>' . PHP_EOL;
			}
			if ($array_evenement[2] == 1){
				$personalisation['sessions'] .= '<strong>Workshop "Introduction aux langages de programmation (Python/Bash)"</strong>, le matin du jeudi 6 février<br>' . PHP_EOL;
			}
			if ($array_evenement[3] == 1){
				$personalisation['sessions'] .= '<strong>Workshop "Requétage"</strong>, le matin du jeudi 6 février<br>' . PHP_EOL;
			}

			//option2
			if ($array_evenement[4] == 1){
				$personalisation['sessions'] .= '<strong>Workshop "Phylogénie"</strong>, l\'après-midi jeudi 6 février<br>' . PHP_EOL;
			}
			if ($array_evenement[5] == 1){
				$personalisation['sessions'] .= '<strong>Workshop "Alignement/Assemblage"</strong>, l\'après-midi jeudi 6 février<br>' . PHP_EOL;
			}
			if ($array_evenement[6] == 1){
				$personalisation['sessions'] .= '<strong>Workshop "Machine Learning"</strong>, l\'après-midi jeudi 6 février<br>' . PHP_EOL;
			}

			//conférence 7 février
			if ($array_evenement[0] == 1){
				$personalisation['sessions'] .= '<strong>Conférences</strong>, le vendredi 7 février<br>' . PHP_EOL;
			}

			$message = sprintfn($message, $personalisation);
			$sujet = 'Confirmation d\'inscription aux Montpellier Omics Days 2020';

			// On envoie le mail
			// Plus besoin de try, les erreurs sont interceptées et envoyées à Sentry silencieusement
			send_mail($mail, $sujet, $message);
			header('Refresh: 3; index.php');
		}
	}
}
else
{
	header('Location:inscription.php?err=2');
}