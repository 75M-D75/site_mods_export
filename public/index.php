<?php
    // Note : aucun contenu avant le ?php pour pouvoir modifier les headers si besoin
    //on inclut le fichier contenant les fonctions php
require("../functions.php");

if( $MAINTEANCE ){
    header('Location: page_not_found.php');
    exit();
}
    //on mets en place le header
echo_header("accueil");
?>
<link rel="stylesheet" type="text/css" href="css/encart.css">
<div id="bloc_box">
    <div class="gradient-border" id="box">
        Vous avez la possibilité de soumettre vos posters, pour cela veuillez envoyer vos abstract à l'adresse
        <br>contact@montpellier-omics-days.fr<br>
    </div>
</div>

<div id="content">
    <div id="third">
        <h3>Dates importantes</h3>
        <div id="liste-dates">
            <ul>
                <li>
                    <div id="date">6 Février 2020</div>
                    <div>Formations unix, Python, R, Requêtage et Machine Learning</div>
                    
                </li>
                <li>
                    <div id="date">7 Février 2020</div>
                    <div>• Conférences dans la journée <br><br> • Soirée vulgarisation à la salle des actes de l’ancienne faculté de médecine.
                    Venez nombreux !</div>
                    
                </li>
                <li id="affiche">
                    <a href="Images/Affiche_MOD2020_VF2.pdf">
                        <img id="thumb-affiche" src="Images/Affiche_MOD2020_VF2.jpg" alt="Affiche des Montpellier Omics Days 2020">
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div id="primary">
        <article>

            <h2>Évolution et analyses multi-omiques </h2>
            
            <p>
                Rassembler les différents acteurs s'intéressant au traitement et à l'analyse des données de séquençage, c'est le but des Montpellier Omics Days. 
                Leur 8ème édition se tiendra les <strong>6 et 7 février 2020</strong> sur le campus du Triolet de l'Université de Montpellier. Elle sera axée sur les dernières technologies de séquençage et d'analyse des génomes ainsi que leurs applications dans la comprehension des phénomènes évolutifs. 
            </p>
            <p>
                Le programme scientifique, détaillé dans l'onglet approprié, est organisé en deux journées : le <strong>jeudi 6 février</strong>, consacré aux différents workshops et le <strong>vendredi 7 février</strong>, dédié aux conférences. <!--(<a href="programme.php">programme</a>).-->
            </p>
            <p>
                <a href="inscription.php">L'inscription</a> pour ces journées est gratuite mais obligatoire pour des raisons de logistique.
            </p>
            <p>
             Aussi, une soirée de vulgarisation, accessible à tous, sera organisée.
         </p>

         
         <div>
            <div class="hover08 column">
              <div>
                <figure class="fig_student">
                   <!-- <a href="Images/etudiants.jpg">  -->
                    <img id="student_img" src="Images/etudiants.jpg" alt="Étudiants M2 BCD et M1 ..."/>
                    <!-- </a>  -->
                </figure>
            </div>
        </div>

        <div id="div-mobil">
            <a href="Images/etudiants.jpg">
                <img style="width:100%" src="Images/etudiants.jpg" 
                alt="Étudiants M2 BCD et M1 ...">
            </a> 
        </div>

        <!-- banniere sous la photo -->

        <div id="container-text-logo-sns">
            <div class="log_text">
                <img id="logo" class="rotate-in-center" src="Images/LOGO_UM_filet-blanc.png"/>
            </div>
            <div class="log_text">
                <h1 id="text-sns" class="text-shadow-pop-br">Masters Sciences et Numérique pour la Santé et Statistique pour les Sciences de la Vie </h1>
            </div>
        </div>

        <!-- <img id="img_sns" src="Images/sns.png"> -->
        
    </div>
    <br/>
    <p> <!-- <small>Présentation des organisateurs de gauche à droite : Clothilde Chenal, Rémy Costa, Manon Knuchel, Gautier Petitjean, Anna Tran, Xavier Mialhe, Nicolas Soirat, Laureline Dejardin Bretones, Clément Bellot, Céline Bial, Besma Boussoufa, Delphine Soulet, Olivier Santt, Morgane Geoffroy, Manon Malestroit, Tchango Idole, Anas Naji, Camille Chantelot, Jean Marone, Gérard Wade, Emira Cherif, Mathys Grapotte</small> -->
        <small>En bas : Gabriel Falque, Jules Romieu
            <br/>
            Au milieu : Anaïs Louis, Gabrielle Pozo, Imane El Atmani, Elodie Simphor, Qiqi He, Lina Mahrach, Sarah Chayrigues, Matthieu Fournel, <strong><a href="https://annasfistonlavier.com/">Anna-Sophie Fiston-Lavier</a></strong> (encadrante et responsable de la formation BCD)
            <br/>

            En haut : Gautier Debaecker, Mathieu Blaison, Xavier Grand, Jérôme Reboul, Nene Djenaba Barry, Gaoussou Sanou, Moussa Samb, Thomas Mion, Mourdas Mohamed, Vincent Wilde
            <br/>
            Ne figurant pas sur la photo : Nastasija Mijovic.
        </small></p>
        <p>

            Les MOD ont la particularité d’être organisés par les étudiants du parcours 
            <a href="https://sns.edu.umontpellier.fr/fr/master-sciences-numerique-pour-la-sante-montpellier/bcd/">“Bioinformatique, Connaissances, Données”</a>
            <a href="http://sns.edu.umontpellier.fr/">Master "Sciences et Numérique pour la Santé"</a> et du 
            <a href="https://formations.umontpellier.fr/fr/formations/sciences-technologies-sante-STS/master-XB/master-mathematiques-program-fruai0342321nprme157/statistique-pour-les-sciences-de-la-vie-subprogram-pr496.html">Master “Statistiques pour les Sciences de la Vie"</a>
            de la <a href="http://sciences.edu.umontpellier.fr/">Faculté des Sciences</a>
            de l'<a href="http://www.umontpellier.fr/">Université de Montpellier</a>
            
            
        </p>  
        <a href="Images/PresentationVulga.pdf">
            <img id="affiche_vulga" src="Images/Presentation_vulga.jpg" alt="Affiche vulgarisation des Montpellier Omics Days 2020">
        </a>

                <!--<p>
                    Juste avant les MOD2018, le congrès Alphy, la rencontre annuelle de Génomique Evolutive, Bioinformatique, Alignement
                    et Phylogénie, se tiendra à Montpellier les 7 et 8 février. N’hésitez pas à coupler les deux évènements et passez
                    ainsi un bon moment scientifique à Montpellier. Pour de plus amples informations, visitez <a href="http://lbbe.univ-lyon1.fr/alphy/">le site de l'évènement</a>.
                </p>

                <p>
                    Attention, pour ces deux manifestations, l’Inscription est gratuite mais obligatoire. 
                    Pour les MOD, vous pouvez vous inscrire <a href="inscription.php">ici</a>.
                </p>-->

                <div id="logo">
                    <!-- <img src="Images/sns.png"> -->
                    <!-- <img src="Images/Logo_BCD.png"> -->
                </div>
            </article>

        </div>

        

        <!--Colonne de droite-->

        <div id="secondary">
            <a class="twitter-timeline" data-lang="fr" data-height="700" data-theme="light" data-link-color="#2B7BB9" href="https://twitter.com/OmicsDays?ref_src=twsrc%5Etfw" >Tweets by OmicsDays</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>

    </div>

    <div id="container-img-students-big" class="outer-div">
        <div class="div-opacity-fond">

        </div>
        <div class="div-stud-in inner-div">
            <figure >
             <img  src="Images/etudiants.jpg" alt="Étudiants M2 BCD et M1 ..."/>
         </figure>
     </div>
 </div>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

 <script type="text/javascript" src="js/gestion_anime.js"></script>

 <?php
 echo_footer();
 // M-D
 ?>
